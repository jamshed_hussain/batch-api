package com.itzst.tracktedu.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@Document
public class Batch {
	@Id
    private String id;
    private String code;
    private String name;
    private String description;
    private boolean enable;
   
    private String courseId;
    private String courseName;

}
